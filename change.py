print ("That would be 372.38kr")
print ("Here, a 1000kr bill...")
cost = float(372.28)
pay = int (1000)
#rounding off the commas
change = (round(pay - cost))
print (change)
# computes how many times the bill "fits" into the original amount
print ("1000kr bills", change//1000)
# % is used to yield the remainder 
change = change%1000
print ("500kr bills", change//500)
change = change%500
print ("200kr bills", change//200)
change = change%200
print ("100kr bills", change//100)
change = change%100
print ("50kr bills", change//50)
change = change%50
print ("20kr bills", change//20)
change = change%20
print ("10kr coins", change//10)
change = change%10
print ("5kr coins", change//5)
change = change%5
print ("2kr coins", change//2)
change = change%2
print ("1kr coins", change//1)