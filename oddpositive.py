import random
random_list = []
# resturns one number in the range of -10 to 10
for i in range (0,1):
   random_number = random.randint(-10,10)
   random_list.append(random_number)

number = sum(random_list)
if number < 0 and (number%2) == 0:
    print (number,"is a negative number and even")

elif number > 0 and (number%2) == 0:
    print (number,"is a positive number and even")

elif number > 0 and (number%2) == 1:
    print (number,"is a positive number and odd")

elif number < 0 and (number%2) == 1:
    print (number,"is a negative number and odd")
    
else:
    print(number,"is a even number and is neither positive nor negative")